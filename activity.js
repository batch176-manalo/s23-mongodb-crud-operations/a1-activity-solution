// create
db.users.insertMany([
    {
        "firstName": "John",
        "lastName": "Smith",
        "email": "jsmith@mail.com",
        "password": "js123",
        "isAdmin": false
    },
    { 
        "firstName": "Peter",
        "lastName": "Patter",
        "email": "pp@mail.com",
        "password": "pp123",
        "isAdmin": false
    },
    { 
        "firstName": "James",
        "lastName": "Parker",
        "email": "jp@mail.com",
        "password": "jp123",
        "isAdmin": false
    },
    { 
        "firstName": "Mark",
        "lastName": "Moor",
        "email": "mm@mail.com",
        "password": "mm123",
        "isAdmin": false
    },
    { 
        "firstName": "Elsa",
        "lastName": "Snow",
        "email": "es@mail.com",
        "password": "es123",
        "isAdmin": false
    },
])



db.courses.insertMany([
    {
        "name": "HTML5",
        "price": 1000,
        "isActive": false,

    },
    {
        "name": "CSS3",
        "price": 1500,
        "isActive": false,

    },
    {
        "name": "Javascript",
        "price": 2000,
        "isActive": false,

    },
])



// Read
db.users.find({"isAdmin": false}) 


// Update
db.users.updateOne({},{$set:{"isAdmin":true}})
db.courses.updateOne({},{$set:{"isActive": true}})


// Delete
db.courses.deleteMany({"isActive": false})


